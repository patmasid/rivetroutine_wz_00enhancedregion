#! /usr/bin/env python
import os,string,random 
import argparse,collections
from array import array
import ROOT
import yoda, sys

def arguments():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-input_dir",  help="directory with yodaXYZ.gz files")
    parser.add_argument("-gen",        help="Generator Powheg or MadGraph or MGaMcAtNlo or Sherpa")
    parser.add_argument("-out_dir",    help="output_directory")
    parser.add_argument("-mc",         help="W<0/T>Z<0/T>_PtZ<ge/lt>150GeV_emu")
    return parser.parse_args()

def fatal_error(msg):
    sys.exit("Fatal error: %s" % msg)

def main():
    
    arg = arguments()
    if(not arg.input_dir):      fatal_error("Please provide -input_dir")
    if(not arg.gen):            fatal_error("Please provide -gen")
    if(not arg.out_dir):        fatal_error("Please provide -out_dir")
    if(not arg.mc):             fatal_error("Please provide -mc")

    input_dir = arg.input_dir
    generator = arg.gen
    out_dir = arg.out_dir
    mc = arg.mc

    command = "yodamerge_tmp.py -o " + os.path.join(out_dir, "Rivet_nominal.yoda") + " " 

    for filename in os.listdir(input_dir):
        if('.yodaXYZ.tgz' in filename):
            dir_name = filename.replace('.yodaXYZ.tgz','.untarred')

            print(os.path.join(input_dir,dir_name))

            os.mkdir(os.path.join(input_dir,dir_name))
            os.chdir(os.path.join(input_dir,dir_name))

            print("cp ../"+filename+" ./")
            print("tar zxvf "+filename)
            print("rm -f "+filename)

            os.system("cp ../"+filename+" ./")
            os.system("tar zxvf "+filename)
            os.system("rm -f "+filename)
            
            command += os.path.join(input_dir,dir_name,"Rivet.yoda")+":1 "
    
    pdf = ""
    if(generator=="Powheg"):
        pdf = "\[_pdfset_260000_\]"
    elif(generator=="MadGraph"):
        pdf = "\[_MUR10_MUF10_PDF260000_\]"
    elif(generator=="MGaMcAtNlo"):
        pdf = "\[_PDF__260000_NNPDF30_nlo_as_0118_\]"
    elif(generator=="Sherpa"):
        pdf = "\[\]"

    command += "-m '.*"+pdf+".*' &> " + os.path.join(out_dir,"out.txt")
    
    os.chdir(out_dir)
    print(command)
    os.system(command)
    
    os.system("sed -i 's/"+pdf+"//g' Rivet_nominal.yoda")

    os.system("python convert2root.py Rivet_nominal.yoda")
    os.system("mv -f Rivet_nominal.root "+mc+".root")
    

if(__name__=="__main__"):
    main()
