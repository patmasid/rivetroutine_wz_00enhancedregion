#! /usr/bin/env python
import os,string,random
import argparse,collections
from array import array
import sys
from datetime import datetime

def arguments():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-input_dir",         help="input directory with the evgen files")
    parser.add_argument("-output_dir",        help="output directory where you want to save the output of the rivet analyses")
    parser.add_argument("-condor_dir",        help="condor directory")
    parser.add_argument("-mc",                help="W<0/T>Z<0/T>_PtZ<ge/lt>150GeV_emu<_0jet/1jet> for LO polarised and WZ_NLO_MGaMcAtNloPy8 for aMC@NLO, WZ_NLO_PowhegPy8 for Powheg+Pythia8, WZ_NLO_Sherpa for Sherpa")
    parser.add_argument("-noFilesPerJob",     help="Number of files per job")
    parser.add_argument("-rivet_routine",     help="ATLAS_WZ_00enhanced_emu for example (rivet routine name)")
    parser.add_argument("-jobOptions_py",     help="path to a template RivetAnalysis python jobOptions file")
    parser.add_argument("-routine_path",      help="path to the .cc and .so files which are named as the input you give for '-rivet_routine' option")
    return parser.parse_args()
    
def fatal_error(msg):
    sys.exit("Fatal error: %s" % msg)

def main():

    arg = arguments()
    if(not arg.input_dir):      fatal_error("Please provide -input_dir")
    if(not arg.output_dir):     fatal_error("Please provide -output_dir")
    if(not arg.condor_dir):     fatal_error("Please provide -condor_dir")
    if(not arg.mc):             fatal_error("Please provide -mc")
    if(not arg.noFilesPerJob):  fatal_error("Please provide -noFilesPerJob")
    if(not arg.rivet_routine):  fatal_error("Please provide -rivet_routine")
    if(not arg.jobOptions_py):  fatal_error("Please provide -jobOptions_py")
    if(not arg.routine_path):   fatal_error("Please provide -routine_path")
    
    input_dir = arg.input_dir
    output_dir = arg.output_dir
    condor_dir = arg.condor_dir
    mc = arg.mc
    noFilesPerJob = int(arg.noFilesPerJob)
    rivet_routine = arg.rivet_routine
    jobOptions_py = arg.jobOptions_py
    routine_path = arg.routine_path

    createFiles(input_dir, output_dir, condor_dir, mc, noFilesPerJob, rivet_routine, jobOptions_py, routine_path)

def createFiles(input_dir, output_dir, condor_dir, mc, noFilesPerJob, rivet_routine, jobOptions_py, routine_path):

    InputCollections = []

    print(input_dir)
    FileList = os.listdir(input_dir)
    
    for file_name in FileList:
        print(file_name)
        if ".root" not in file_name:
            continue
        inputFile = os.path.join(input_dir,file_name)
        if os.path.exists(inputFile):
            InputCollections.append(inputFile)

    num_files = len(InputCollections)
    print("num_files: ", num_files)

    num_jobs = -1
    remain = num_files%noFilesPerJob
    
    if(remain > 0):
        num_jobs = int(num_files/noFilesPerJob) + 1
    else:
        num_jobs = int(num_files/noFilesPerJob)

    InputCollections_final = collections.OrderedDict()
    
    if(not os.path.isdir(output_dir)):
        os.makedirs(output_dir)

    runs_dir = os.path.join(output_dir, "runs")
    yoda_dir = os.path.join(output_dir, "yodas")
    os.mkdir(runs_dir)
    os.mkdir(yoda_dir)

    print("num jobs: ", num_jobs)

    for ijob in range(num_jobs):
        
        run_dir = os.path.join(runs_dir, mc+"_"+str(ijob))
        os.mkdir(os.path.join(runs_dir, mc+"_"+str(ijob)))
        
        if(ijob != num_jobs-1):
            InputCollections_final[ijob] = InputCollections[ijob*noFilesPerJob:(ijob+1)*noFilesPerJob]
        else:
            InputCollections_final[num_jobs-1] = InputCollections[ijob*noFilesPerJob:]
        
    for i_job in range(num_jobs):
        run_dir = os.path.join(runs_dir, mc+"_"+str(i_job))

        jobOpt_temp = open(jobOptions_py,"r")
        jobOpt_lines = jobOpt_temp.readlines()
        
        jobOpt = open(os.path.join(run_dir,"RivetAnalysis_JO_Rivet3_"+mc+"_"+str(i_job)+".py"),"w")
        
        list_evgenfiles = "svcMgr.EventSelector.InputCollections = ["
        for ifile in range(len(InputCollections_final[i_job])):
            if(ifile != len(InputCollections_final[i_job])-1):
                list_evgenfiles += "'"+InputCollections_final[i_job][ifile]+"',"
            else:
                list_evgenfiles += "'"+InputCollections_final[i_job][ifile]+"']\n"

        for line in jobOpt_lines:
            
            if('svcMgr.EventSelector.InputCollections = ["None"]' in line):
                jobOpt.write(list_evgenfiles)
            elif('analyses= "ANALYSIS"' in line):
                jobOpt.write('analyses= "'+rivet_routine+'"\n')
            else:
                jobOpt.write(line)

        os.system("cp "+routine_path+"/*.* "+run_dir+"/")
        
        exe_file = open(os.path.join(run_dir,"run.sh"),"w")
        exe_file.write("#!/bin/sh\n")
        exe_file.write("source /afs/atlas.umich.edu/home/patmasid/public/setupSystematicsTool_condor.sh\n")
        exe_file.write("source /afs/atlas.umich.edu/home/patmasid/public/setupSystematics_LHAPDF.sh\n")
        exe_file.write("cd "+run_dir+"\n")
        exe_file.write("athena "+"RivetAnalysis_JO_Rivet3_"+mc+"_"+str(i_job)+".py\n")
        yoda_tgz = "Rivet3_"+mc+"_"+str(i_job)+".yodaXYZ.tgz"
        exe_file.write("tar -czvf "+yoda_tgz+" Rivet.yoda\n")
        exe_file.write("cp "+yoda_tgz+" "+yoda_dir+"/\n")
        exe_file.close()

        os.system("chmod +x "+os.path.join(run_dir,"run.sh"))
        
    if(not os.path.isdir(condor_dir)):
        os.makedirs(condor_dir)

    time_dir = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')+"_"+mc
    if(not os.path.isdir( os.path.join(condor_dir,time_dir) )):
        os.mkdir(os.path.join(condor_dir,time_dir))

    createCondorFiles(runs_dir, mc, os.path.join(condor_dir,time_dir), num_jobs)

def createCondorFiles(runs_dir, mc, time_dir, num_jobs):

    o_condor_dir = time_dir
    f_jobdesc_submit_path = os.path.join(o_condor_dir,"jobdesc_"+mc+".submit")
    
    print(f_jobdesc_submit_path)

    run_file = os.path.join(runs_dir, mc+"_$(ProcId)","run.sh")
    
    condor_job_file = open(f_jobdesc_submit_path, 'w')
    condor_job_file.write("Executable      =   "+run_file+"\n")
    condor_job_file.write("arguments   =   \n")
    condor_job_file.write("Log     =       "+os.path.join(o_condor_dir,"job_$(ProcId).log")+"\n")
    condor_job_file.write("Output  =       "+os.path.join(o_condor_dir,"outputfile_$(ProcId)")+"\n")
    condor_job_file.write("Error   =       "+os.path.join(o_condor_dir,"error_$(ProcId).err")+"\n")
    condor_job_file.write("")
    condor_job_file.write("Queue "+str(num_jobs))
    
    condor_job_file.close()
    
    # write script for submitting jobs                                                                                                                                                              
    #f_submit_jobs = os.path.join(condor_dir,"submit_jobs.sh")
    #submit_jobs_file = open(f_submit_jobs, 'w')
    #submit_jobs_file.write("condor_submit "+f_jobdesc_submit_path)
    #submit_jobs_file.close()

    print(time_dir)
    print("condor_submit "+f_jobdesc_submit_path)

    os.chdir(time_dir)
    os.system("condor_submit "+f_jobdesc_submit_path)

if(__name__=="__main__"):
    main()
