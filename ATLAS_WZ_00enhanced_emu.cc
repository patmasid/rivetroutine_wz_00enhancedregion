// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/NonHadronicFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include <Rivet/Math/LorentzTrans.hh>
#include <Rivet/Math/Vector4.hh>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <cmath>

//#include "TFile.h"

//#include "TTree.h"

namespace Rivet {


  /// @brief Measurement of the WZ production cross section at 13 TeV
  class ATLAS_WZ_00enhanced_emu : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_WZ_00enhanced_emu);

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Get photons to dress leptons
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);

      // Electrons and muons in Fiducial PS
      PromptFinalState leptons(Cuts::abspid == PID::ELECTRON || Cuts::abspid == PID::MUON);
      leptons.acceptTauDecays(false);
      DressedLeptons dressedleptons(photons, leptons, 0.1, Cuts::open(), true);
      declare(dressedleptons, "DressedLeptons");

      // Prompt neutrinos (yikes!)
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(false);
      declare(neutrinos, "Neutrinos");
      MSG_WARNING("\033[91;1mLIMITED VALIDITY - check info file for details!\033[m");

      // Jets
      /*VetoedFinalState veto;
      veto.addVetoOnThisFinalState(dressedleptons);
      FastJets jets(veto, FastJets::ANTIKT, 0.4);
      declare(jets, "Jets");*/
     
      // Muons
      PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true); // true = use muons from prompt tau decays
      DressedLeptons all_dressed_mu(photons, bare_mu, 0.1, Cuts::abseta < 2.5, true);

      // Electrons                                                                                                                                                                                                 
      PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true); // true = use electrons from prompt tau decays
      DressedLeptons all_dressed_el(photons, bare_el, 0.1, Cuts::abseta < 2.5, true);

      //Jet forming                                                                                                                                                                                                
      VetoedFinalState vfs(FinalState(Cuts::abseta < 5.0));
      vfs.addVetoOnThisFinalState(all_dressed_el);
      vfs.addVetoOnThisFinalState(all_dressed_mu);

      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "Jets");

      MSG_INFO( "MSG_INFO: Book histograms");
      MSG_DEBUG( "MSG_DEBUG: Book histograms");
      //MSG_VERBOSE("MSG_VERBOSE: Book histograms");

      //MSG_INFO( "MSG_INFO: sumw" << sumW());
      
      book(h_Cut0_WZ_pT,"Cut0_WZ_pT",40,0,200);
      book(h_Cut0_pT_W,"Cut0_pT_W",60,0,600);
      book(h_Cut0_pT_Z,"Cut0_pT_Z",pT_Z_CUTS);
      book(h_Cut0_M_WZ,"Cut0_M_WZ",50,30,1000);
      book(h_Cut0_M_Z,"Cut0_M_Z",15,80,110);
      book(h_Cut0_M_3l,"Cut0_M_3l",50,30,200);
      book(h_Cut0_mT_WZ,"Cut0_mT_WZ",50,0,1000);
      book(h_Cut0_mT_W,"Cut0_mT_W",10,0,200);
      book(h_Cut0_MET,"Cut0_MET",10,0,420);
      book(h_Cut0_R21,"Cut0_R21",20,0,1);
      book(h_Cut0_DY_WZ,"Cut0_DY_WZ",12,-3,3);
      book(h_Cut0_DY_3Z,"Cut0_DY_3Z",20,-6,6);
      book(h_Cut0_DY_3Z_WLZL,"Cut0_DY_3Z_WLZL",20,-6,6);
      book(h_Cut0_DY_3Z_WLZH,"Cut0_DY_3Z_WLZH",20,-6,6);
      book(h_Cut0_DY_3Z_WHZL,"Cut0_DY_3Z_WHZL",20,-6,6);
      book(h_Cut0_DY_3Z_WHZH,"Cut0_DY_3Z_WHZH",20,-6,6);
      book(h_Cut0_DY_3N,"Cut0_DY_3N",20,-6,6);
      book(h_Cut0_CosTheta_W_Lab,"Cut0_CosTheta_W_Lab",20,-1,1);
      book(h_Cut0_CosTheta_Z_Lab,"Cut0_CosTheta_Z_Lab",20,-1,1);
      book(h_Cut0_CosTheta_W_WZ,"Cut0_CosTheta_W_WZ",20,-1,1);
      book(h_Cut0_CosTheta_W_WZ_WLZL,"Cut0_CosTheta_W_WZ_WLZL",20,-1,1);
      book(h_Cut0_CosTheta_W_WZ_WLZH,"Cut0_CosTheta_W_WZ_WLZH",20,-1,1);
      book(h_Cut0_CosTheta_W_WZ_WHZL,"Cut0_CosTheta_W_WZ_WHZL",20,-1,1);
      book(h_Cut0_CosTheta_W_WZ_WHZH,"Cut0_CosTheta_W_WZ_WHZH",20,-1,1);
      book(h_Cut0_CosTheta_Z_WZ,"Cut0_CosTheta_Z_WZ",20,-1,1);
      book(h_Cut0_CosTheta_WLep_Wrest,"Cut0_CosTheta_WLep_Wrest",10,-1,1);
      book(h_Cut0_CosTheta_WLep_Wrest_wWZrest,"Cut0_CosTheta_WLep_Wrest_wWZrest",10,-1,1);
      book(h_Cut0_CosTheta_ZLepM_Zrest,"Cut0_CosTheta_ZLepM_Zrest",10,-1,1);
      book(h_Cut0_CosTheta_ZLepM_Zrest_wWZrest,"Cut0_CosTheta_ZLepM_Zrest_wWZrest",10,-1,1);
      book(h_Cut0_DeltaPhiLepWLepZ,"Cut0_DeltaPhiLepWLepZ",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZ_WLZL,"Cut0_DeltaPhiLepWLepZ_WLZL",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZ_WLZH,"Cut0_DeltaPhiLepWLepZ_WLZH",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZ_WHZL,"Cut0_DeltaPhiLepWLepZ_WHZL",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZ_WHZH,"Cut0_DeltaPhiLepWLepZ_WHZH",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZWZframe,"Cut0_DeltaPhiLepWLepZWZframe",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZWZframe_WLZL,"Cut0_DeltaPhiLepWLepZWZframe_WLZL",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZWZframe_WLZH,"Cut0_DeltaPhiLepWLepZWZframe_WLZH",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZWZframe_WHZL,"Cut0_DeltaPhiLepWLepZWZframe_WHZL",40,-M_PI,M_PI);
      book(h_Cut0_DeltaPhiLepWLepZWZframe_WHZH,"Cut0_DeltaPhiLepWLepZWZframe_WHZH",40,-M_PI,M_PI);
      book(h_Cut0_all_Lep_pT,"Cut0_all_Lep_pT",40,20,420);
      book(h_Cut0_all_Lep_Eta,"Cut0_all_Lep_Eta",27,-2.7,2.7);
      book(h_Cut0_all_Lep_Phi,"Cut0_all_Lep_Phi",20,-M_PI,M_PI);

      book(h_Cut1_WZ_pT,"Cut1_WZ_pT",40,0,200);
      book(h_Cut1_pT_W,"Cut1_pT_W",60,0,600);
      book(h_Cut1_pT_Z,"Cut1_pT_Z",pT_Z_CUTS);
      book(h_Cut1_M_WZ,"Cut1_M_WZ",50,30,1000);
      book(h_Cut1_M_Z,"Cut1_M_Z",15,80,110);
      book(h_Cut1_M_3l,"Cut1_M_3l",50,30,200);
      book(h_Cut1_mT_WZ,"Cut1_mT_WZ",50,0,1000);
      book(h_Cut1_mT_W,"Cut1_mT_W",10,0,200);
      book(h_Cut1_MET,"Cut1_MET",10,0,420);
      book(h_Cut1_R21,"Cut1_R21",20,0,1);
      book(h_Cut1_DY_WZ,"Cut1_DY_WZ",12,-3,3);
      book(h_Cut1_DY_3Z,"Cut1_DY_3Z",20,-6,6);
      book(h_Cut1_DY_3Z_WLZL,"Cut1_DY_3Z_WLZL",20,-6,6);
      book(h_Cut1_DY_3Z_WLZH,"Cut1_DY_3Z_WLZH",20,-6,6);
      book(h_Cut1_DY_3Z_WHZL,"Cut1_DY_3Z_WHZL",20,-6,6);
      book(h_Cut1_DY_3Z_WHZH,"Cut1_DY_3Z_WHZH",20,-6,6);
      book(h_Cut1_DY_3N,"Cut1_DY_3N",20,-6,6);
      book(h_Cut1_CosTheta_W_Lab,"Cut1_CosTheta_W_Lab",20,-1,1);
      book(h_Cut1_CosTheta_Z_Lab,"Cut1_CosTheta_Z_Lab",20,-1,1);
      book(h_Cut1_CosTheta_W_WZ,"Cut1_CosTheta_W_WZ",20,-1,1);
      book(h_Cut1_CosTheta_W_WZ_WLZL,"Cut1_CosTheta_W_WZ_WLZL",20,-1,1);
      book(h_Cut1_CosTheta_W_WZ_WLZH,"Cut1_CosTheta_W_WZ_WLZH",20,-1,1);
      book(h_Cut1_CosTheta_W_WZ_WHZL,"Cut1_CosTheta_W_WZ_WHZL",20,-1,1);
      book(h_Cut1_CosTheta_W_WZ_WHZH,"Cut1_CosTheta_W_WZ_WHZH",20,-1,1);
      book(h_Cut1_CosTheta_Z_WZ,"Cut1_CosTheta_Z_WZ",20,-1,1);
      book(h_Cut1_CosTheta_WLep_Wrest,"Cut1_CosTheta_WLep_Wrest",10,-1,1);
      book(h_Cut1_CosTheta_WLep_Wrest_wWZrest,"Cut1_CosTheta_WLep_Wrest_wWZrest",10,-1,1);
      book(h_Cut1_CosTheta_ZLepM_Zrest,"Cut1_CosTheta_ZLepM_Zrest",10,-1,1);
      book(h_Cut1_CosTheta_ZLepM_Zrest_wWZrest,"Cut1_CosTheta_ZLepM_Zrest_wWZrest",10,-1,1);
      book(h_Cut1_DeltaPhiLepWLepZ,"Cut1_DeltaPhiLepWLepZ",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZ_WLZL,"Cut1_DeltaPhiLepWLepZ_WLZL",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZ_WLZH,"Cut1_DeltaPhiLepWLepZ_WLZH",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZ_WHZL,"Cut1_DeltaPhiLepWLepZ_WHZL",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZ_WHZH,"Cut1_DeltaPhiLepWLepZ_WHZH",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZWZframe,"Cut1_DeltaPhiLepWLepZWZframe",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZWZframe_WLZL,"Cut1_DeltaPhiLepWLepZWZframe_WLZL",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZWZframe_WLZH,"Cut1_DeltaPhiLepWLepZWZframe_WLZH",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZWZframe_WHZL,"Cut1_DeltaPhiLepWLepZWZframe_WHZL",40,-M_PI,M_PI);
      book(h_Cut1_DeltaPhiLepWLepZWZframe_WHZH,"Cut1_DeltaPhiLepWLepZWZframe_WHZH",40,-M_PI,M_PI);
      book(h_Cut1_all_Lep_pT,"Cut1_all_Lep_pT",40,20,420);
      book(h_Cut1_all_Lep_Eta,"Cut1_all_Lep_Eta",27,-2.7,2.7);
      book(h_Cut1_all_Lep_Phi,"Cut1_all_Lep_Phi",20,-M_PI,M_PI);
      
      book(h_Cut2_WZ_pT,"Cut2_WZ_pT",40,0,200);
      book(h_Cut2_pT_W,"Cut2_pT_W",60,0,600);
      book(h_Cut2_pT_Z,"Cut2_pT_Z",pT_Z_CUTS);
      book(h_Cut2_M_WZ,"Cut2_M_WZ",50,30,1000);
      book(h_Cut2_M_Z,"Cut2_M_Z",15,80,110);
      book(h_Cut2_M_3l,"Cut2_M_3l",50,30,200);
      book(h_Cut2_mT_WZ,"Cut2_mT_WZ",50,0,1000);
      book(h_Cut2_mT_W,"Cut2_mT_W",10,0,200);
      book(h_Cut2_MET,"Cut2_MET",10,0,420);
      book(h_Cut2_R21,"Cut2_R21",20,0,1);
      book(h_Cut2_DY_WZ,"Cut2_DY_WZ",12,-3,3);
      book(h_Cut2_DY_3Z,"Cut2_DY_3Z",20,-6,6);
      book(h_Cut2_DY_3Z_WLZL,"Cut2_DY_3Z_WLZL",20,-6,6);
      book(h_Cut2_DY_3Z_WLZH,"Cut2_DY_3Z_WLZH",20,-6,6);
      book(h_Cut2_DY_3Z_WHZL,"Cut2_DY_3Z_WHZL",20,-6,6);
      book(h_Cut2_DY_3Z_WHZH,"Cut2_DY_3Z_WHZH",20,-6,6);
      book(h_Cut2_DY_3N,"Cut2_DY_3N",20,-6,6);
      book(h_Cut2_CosTheta_W_Lab,"Cut2_CosTheta_W_Lab",20,-1,1);
      book(h_Cut2_CosTheta_Z_Lab,"Cut2_CosTheta_Z_Lab",20,-1,1);
      book(h_Cut2_CosTheta_W_WZ,"Cut2_CosTheta_W_WZ",20,-1,1);
      book(h_Cut2_CosTheta_W_WZ_WLZL,"Cut2_CosTheta_W_WZ_WLZL",20,-1,1);
      book(h_Cut2_CosTheta_W_WZ_WLZH,"Cut2_CosTheta_W_WZ_WLZH",20,-1,1);
      book(h_Cut2_CosTheta_W_WZ_WHZL,"Cut2_CosTheta_W_WZ_WHZL",20,-1,1);
      book(h_Cut2_CosTheta_W_WZ_WHZH,"Cut2_CosTheta_W_WZ_WHZH",20,-1,1);
      book(h_Cut2_CosTheta_Z_WZ,"Cut2_CosTheta_Z_WZ",20,-1,1);
      book(h_Cut2_CosTheta_WLep_Wrest,"Cut2_CosTheta_WLep_Wrest",10,-1,1);
      book(h_Cut2_CosTheta_WLep_Wrest_wWZrest,"Cut2_CosTheta_WLep_Wrest_wWZrest",10,-1,1);
      book(h_Cut2_CosTheta_ZLepM_Zrest,"Cut2_CosTheta_ZLepM_Zrest",10,-1,1);
      book(h_Cut2_CosTheta_ZLepM_Zrest_wWZrest,"Cut2_CosTheta_ZLepM_Zrest_wWZrest",10,-1,1);
      book(h_Cut2_DeltaPhiLepWLepZ,"Cut2_DeltaPhiLepWLepZ",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZ_WLZL,"Cut2_DeltaPhiLepWLepZ_WLZL",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZ_WLZH,"Cut2_DeltaPhiLepWLepZ_WLZH",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZ_WHZL,"Cut2_DeltaPhiLepWLepZ_WHZL",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZ_WHZH,"Cut2_DeltaPhiLepWLepZ_WHZH",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZWZframe,"Cut2_DeltaPhiLepWLepZWZframe",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZWZframe_WLZL,"Cut2_DeltaPhiLepWLepZWZframe_WLZL",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZWZframe_WLZH,"Cut2_DeltaPhiLepWLepZWZframe_WLZH",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZWZframe_WHZL,"Cut2_DeltaPhiLepWLepZWZframe_WHZL",40,-M_PI,M_PI);
      book(h_Cut2_DeltaPhiLepWLepZWZframe_WHZH,"Cut2_DeltaPhiLepWLepZWZframe_WHZH",40,-M_PI,M_PI);
      book(h_Cut2_all_Lep_pT,"Cut2_all_Lep_pT",40,20,420);
      book(h_Cut2_all_Lep_Eta,"Cut2_all_Lep_Eta",27,-2.7,2.7);
      book(h_Cut2_all_Lep_Phi,"Cut2_all_Lep_Phi",20,-M_PI,M_PI);

      book(h_Cut3_WZ_pT,"Cut3_WZ_pT",40,0,200);
      book(h_Cut3_pT_W,"Cut3_pT_W",60,0,600);
      book(h_Cut3_pT_Z,"Cut3_pT_Z",pT_Z_CUTS);
      book(h_Cut3_M_WZ,"Cut3_M_WZ",50,30,1000);
      book(h_Cut3_M_Z,"Cut3_M_Z",15,80,110);
      book(h_Cut3_M_3l,"Cut3_M_3l",50,30,200);
      book(h_Cut3_mT_WZ,"Cut3_mT_WZ",50,0,1000);
      book(h_Cut3_mT_W,"Cut3_mT_W",10,0,200);
      book(h_Cut3_MET,"Cut3_MET",10,0,420);
      book(h_Cut3_R21,"Cut3_R21",20,0,1);
      book(h_Cut3_DY_WZ,"Cut3_DY_WZ",12,-3,3);
      book(h_Cut3_DY_3Z,"Cut3_DY_3Z",20,-6,6);
      book(h_Cut3_DY_3Z_WLZL,"Cut3_DY_3Z_WLZL",20,-6,6);
      book(h_Cut3_DY_3Z_WLZH,"Cut3_DY_3Z_WLZH",20,-6,6);
      book(h_Cut3_DY_3Z_WHZL,"Cut3_DY_3Z_WHZL",20,-6,6);
      book(h_Cut3_DY_3Z_WHZH,"Cut3_DY_3Z_WHZH",20,-6,6);
      book(h_Cut3_DY_3N,"Cut3_DY_3N",20,-6,6);
      book(h_Cut3_CosTheta_W_Lab,"Cut3_CosTheta_W_Lab",20,-1,1);
      book(h_Cut3_CosTheta_Z_Lab,"Cut3_CosTheta_Z_Lab",20,-1,1);
      book(h_Cut3_CosTheta_W_WZ,"Cut3_CosTheta_W_WZ",20,-1,1);
      book(h_Cut3_CosTheta_W_WZ_WLZL,"Cut3_CosTheta_W_WZ_WLZL",20,-1,1);
      book(h_Cut3_CosTheta_W_WZ_WLZH,"Cut3_CosTheta_W_WZ_WLZH",20,-1,1);
      book(h_Cut3_CosTheta_W_WZ_WHZL,"Cut3_CosTheta_W_WZ_WHZL",20,-1,1);
      book(h_Cut3_CosTheta_W_WZ_WHZH,"Cut3_CosTheta_W_WZ_WHZH",20,-1,1);
      book(h_Cut3_CosTheta_Z_WZ,"Cut3_CosTheta_Z_WZ",20,-1,1);
      book(h_Cut3_CosTheta_WLep_Wrest,"Cut3_CosTheta_WLep_Wrest",10,-1,1);
      book(h_Cut3_CosTheta_WLep_Wrest_wWZrest,"Cut3_CosTheta_WLep_Wrest_wWZrest",10,-1,1);
      book(h_Cut3_CosTheta_ZLepM_Zrest,"Cut3_CosTheta_ZLepM_Zrest",10,-1,1);
      book(h_Cut3_CosTheta_ZLepM_Zrest_wWZrest,"Cut3_CosTheta_ZLepM_Zrest_wWZrest",10,-1,1);
      book(h_Cut3_DeltaPhiLepWLepZ,"Cut3_DeltaPhiLepWLepZ",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZ_WLZL,"Cut3_DeltaPhiLepWLepZ_WLZL",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZ_WLZH,"Cut3_DeltaPhiLepWLepZ_WLZH",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZ_WHZL,"Cut3_DeltaPhiLepWLepZ_WHZL",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZ_WHZH,"Cut3_DeltaPhiLepWLepZ_WHZH",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZWZframe,"Cut3_DeltaPhiLepWLepZWZframe",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZWZframe_WLZL,"Cut3_DeltaPhiLepWLepZWZframe_WLZL",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZWZframe_WLZH,"Cut3_DeltaPhiLepWLepZWZframe_WLZH",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZWZframe_WHZL,"Cut3_DeltaPhiLepWLepZWZframe_WHZL",40,-M_PI,M_PI);
      book(h_Cut3_DeltaPhiLepWLepZWZframe_WHZH,"Cut3_DeltaPhiLepWLepZWZframe_WHZH",40,-M_PI,M_PI);
      book(h_Cut3_all_Lep_pT,"Cut3_all_Lep_pT",40,20,420);
      book(h_Cut3_all_Lep_Eta,"Cut3_all_Lep_Eta",27,-2.7,2.7);
      book(h_Cut3_all_Lep_Phi,"Cut3_all_Lep_Phi",20,-M_PI,M_PI);

      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {
	PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);
	_processedEvents[idx]=0;
	_eventsPassCuts[idx]=0;
      }

      m_strOutput.open("output.txt");

    }

    double Phi_mpi_to_pi(double x) {
      if(isNaN(x)){
	MSG_ERROR("Phi_mpi_to_pi: function called with NaN");
	return x;
      }
      while (x >= M_PI) x -= 2*M_PI;
      while (x < -M_PI) x += 2*M_PI;
      return x;
    }

    void analyze(const Event& event) {

      // Selection cuts                                         
      bool _passCuts[ps_NCuts];                                                 
      for (int ps = ps_PreFilter; ps <= ps_NCuts; ps++) {                                                              
	PhaseSpaceSelections idx = static_cast<PhaseSpaceSelections>(ps);                                  
	_passCuts[idx] = false;                                                                                        
      }  

      _passCuts[ps_PreFilter] = true;

      _eventsPassCuts[ps_PreFilter] += 1;

      const vector<DressedLepton>& dressedleptons = apply<DressedLeptons>(event, "DressedLeptons").dressedLeptons();
      const Particles& neutrinos = apply<PromptFinalState>(event, "Neutrinos").particlesByPt();
      Jets jets = apply<JetAlg>(event, "Jets").jetsByPt();

      int i, j, k;
      double MassZ01 = 0., MassZ02 = 0., MassZ12 = 0.;
      double MassW0 = 0., MassW1 = 0., MassW2 = 0.;
      double WeightZ1, WeightZ2, WeightZ3;
      double WeightW1, WeightW2, WeightW3;
      double M1, M2, M3;
      double WeightTotal1, WeightTotal2, WeightTotal3;

      //---Fiducial PS: assign leptons to W and Z bosons using Resonant shape algorithm
      if (dressedleptons.size() < 3 || neutrinos.size() < 1)  vetoEvent;

      int EventType = -1;
      int Nel = 0, Nmu = 0;

      for (const DressedLepton& l : dressedleptons) {
        if (l.abspid() == 11)  ++Nel;
        if (l.abspid() == 13)  ++Nmu;
      }

      if ( (Nel == 3)  && (Nmu==0) )  EventType = 3;
      if ( (Nel == 2)  && (Nmu==1) )  EventType = 2;
      if ( (Nel == 1)  && (Nmu==2) )  EventType = 1;
      if ( (Nel == 0)  && (Nmu==3) )  EventType = 0;

      int EventCharge = -dressedleptons[0].charge() * dressedleptons[1].charge() * dressedleptons[2].charge();

      MassZ01 = 0; MassZ02 = 0; MassZ12 = 0;
      MassW0 = 0;  MassW1 = 0;  MassW2 = 0;

      int icomb = 0;	
      // try Z pair of leptons 01
      if ( (dressedleptons[0].pid() == -(dressedleptons[1].pid())) && (dressedleptons[2].pid()*neutrinos[0].pid()< 0) && (dressedleptons[2].abspid()==neutrinos[0].abspid()-1) ) {
        MassZ01 = (dressedleptons[0].momentum() + dressedleptons[1].momentum()).mass();
        MassW2 = (dressedleptons[2].momentum() + neutrinos[0].momentum()).mass();
	icomb = 1;
      }
      // try Z pair of leptons 02
      if ( (dressedleptons[0].pid() == -(dressedleptons[2].pid())) && (dressedleptons[1].pid()*neutrinos[0].pid()< 0) && (dressedleptons[1].abspid()==neutrinos[0].abspid()-1) ) {
        MassZ02 = (dressedleptons[0].momentum() + dressedleptons[2].momentum()).mass();
        MassW1 = (dressedleptons[1].momentum() + neutrinos[0].momentum()).mass();
	icomb = 2;
      }
      // try Z pair of leptons 12
      if ( (dressedleptons[1].pid() == -(dressedleptons[2].pid())) && (dressedleptons[0].pid()*neutrinos[0].pid()< 0) && (dressedleptons[0].abspid()==neutrinos[0].abspid()-1) ) {
        MassZ12 = (dressedleptons[1].momentum() + dressedleptons[2].momentum()).mass();
        MassW0 = (dressedleptons[0].momentum() + neutrinos[0].momentum()).mass();
	icomb = 3;
      }
      
      if(icomb<=0) vetoEvent;

      WeightZ1 = 1/(pow(MassZ01*MassZ01 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW1 = 1/(pow(MassW2*MassW2 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      WeightTotal1 = WeightZ1*WeightW1;
      M1 = -1*WeightTotal1;

      WeightZ2 = 1/(pow(MassZ02*MassZ02- MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW2 = 1/(pow(MassW1*MassW1- MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      WeightTotal2 = WeightZ2*WeightW2;
      M2 = -1*WeightTotal2;

      WeightZ3 = 1/(pow(MassZ12*MassZ12 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
      WeightW3 = 1/(pow(MassW0*MassW0 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
      WeightTotal3 = WeightZ3*WeightW3;
      M3 = -1*WeightTotal3;

      if( (M1 < M2 && M1 < M3) || (MassZ01 != 0 && MassW2 != 0 && MassZ02 == 0 && MassZ12 == 0) ) {
        i = 0; j = 1; k = 2;
      }
      if((M2 < M1 && M2 < M3) || (MassZ02 != 0 && MassW1 != 0 && MassZ01 == 0 && MassZ12 == 0) ) {
        i = 0; j = 2; k = 1;
      }
      if((M3 < M1 && M3 < M2) || (MassZ12 != 0 && MassW0 != 0 && MassZ01 == 0 && MassZ02 == 0) ) {
        i = 1; j = 2; k = 0;
      }

      DressedLepton Z_Lep_1 = dressedleptons[i];
      DressedLepton Z_Lep_2 = dressedleptons[j];
      DressedLepton W_Lep = dressedleptons[k];

      FourMomentum Zlepton1 = dressedleptons[i].momentum();
      FourMomentum Zlepton2 = dressedleptons[j].momentum();
      FourMomentum Wlepton  = dressedleptons[k].momentum();
      FourMomentum Zboson   = dressedleptons[i].momentum()+dressedleptons[j].momentum();
      FourMomentum Wboson   = dressedleptons[k].momentum()+neutrinos[0].momentum();

      double pT_WZ, pT_Z, pT_W, MET;
      pT_W = Wboson.pT();
      pT_Z = Zboson.pT();
      pT_WZ = (Wboson+Zboson).pT();
      MET = ( (dressedleptons[k].momentum()+dressedleptons[i].momentum()+dressedleptons[j].momentum()).pT() );

      double WZ_pt = Zlepton1.pt() + Zlepton2.pt() + Wlepton.pt() + neutrinos[0].pt();
      double WZ_px = Zlepton1.px() + Zlepton2.px() + Wlepton.px() + neutrinos[0].px();
      double WZ_py = Zlepton1.py() + Zlepton2.py() + Wlepton.py() + neutrinos[0].py();
      double mTWZ = sqrt( pow(WZ_pt, 2) - ( pow(WZ_px, 2) + pow(WZ_py,2) ) )/GeV;

      double cosLepNeut;
      double norm = Wlepton.pT() * neutrinos[0].pt();
      double mTW = 0;
      if(norm != 0){
	cosLepNeut = ( Wlepton.px()*neutrinos[0].px() + Wlepton.py()*neutrinos[0].py() )/norm ;
	if(1-cosLepNeut >= 0 ) mTW = sqrt( 2 * Wlepton.pT() * neutrinos[0].pt() * (1-cosLepNeut ) );
      }

      //========================================================================================================//
      //=========================== Using RIVET to calculate all the kinematic variables =======================//
      //========================================================================================================//
      
      // For kinematic variable calculations
      FourMomentum WZ, W_WZframe, Z_WZframe, Wlep_WZframe, ZlepM_WZframe, ZlepP_WZframe;
      FourMomentum Wlep_Wframe, Wlep_Wframe_WZ, ZlepM_Zframe, ZlepM_Zframe_WZ, ZlepP_Zframe, ZlepP_Zframe_WZ;

      WZ = Zboson+Wboson;

      W_WZframe = Wboson;
      Z_WZframe = Zboson;
      Wlep_WZframe = Wlepton;

      FourMomentum ZleptonM, ZleptonP;

      if(Z_Lep_1.pid()>0){
        ZleptonM = Zlepton1;
        ZleptonP = Zlepton2;
      }
      else{
        ZleptonM = Zlepton2;
        ZleptonP = Zlepton1;
      }

      ZlepM_WZframe = ZleptonM;
      ZlepP_WZframe = ZleptonP;
      
      LorentzTransform LT_WZ = LorentzTransform();
      LT_WZ = LT_WZ.mkObjTransformFromBeta(-(WZ.betaVec()));
      
      W_WZframe = LT_WZ.transform(W_WZframe);
      Z_WZframe = LT_WZ.transform(Z_WZframe);
      Wlep_WZframe = LT_WZ.transform(Wlep_WZframe);
      ZlepM_WZframe = LT_WZ.transform(ZlepM_WZframe);
      ZlepP_WZframe = LT_WZ.transform(ZlepP_WZframe);

      Wlep_Wframe = Wlepton;
      Wlep_Wframe_WZ = Wlep_WZframe;
      ZlepM_Zframe = ZleptonM;
      ZlepM_Zframe_WZ = ZlepM_WZframe;
      ZlepP_Zframe = ZleptonP;
      ZlepP_Zframe_WZ = ZlepP_WZframe;

      LorentzTransform LT_W = LorentzTransform();
      LT_W = LT_W.mkObjTransformFromBeta(-(Wboson.betaVec()));

      LorentzTransform LT_Z = LorentzTransform();
      LT_Z = LT_Z.mkObjTransformFromBeta(-(Zboson.betaVec()));

      Wlep_Wframe = LT_W.transform(Wlep_Wframe);
      ZlepM_Zframe = LT_Z.transform(ZlepM_Zframe);
      ZlepP_Zframe = LT_Z.transform(ZlepP_Zframe);

      LorentzTransform LT_W_WZ = LorentzTransform();
      LT_W_WZ = LT_W_WZ.mkObjTransformFromBeta(-(W_WZframe.betaVec()));

      LorentzTransform LT_Z_WZ = LorentzTransform();
      LT_Z_WZ = LT_Z_WZ.mkObjTransformFromBeta(-(Z_WZframe.betaVec()));

      Wlep_Wframe_WZ = LT_W_WZ.transform(Wlep_Wframe_WZ);
      ZlepM_Zframe_WZ = LT_Z_WZ.transform(ZlepM_Zframe_WZ);
      ZlepP_Zframe_WZ = LT_Z_WZ.transform(ZlepP_Zframe_WZ);

      double R21 = 0.0;
      if(pT_W < pT_Z) R21 = pT_W/pT_Z;
      else R21 = pT_Z/pT_W;

      double CosTheta_W_Lab = cos(Wboson.theta());
      double CosTheta_Z_Lab = cos(Zboson.theta());
      double CosTheta_W_WZ = cos(W_WZframe.theta());
      double CosTheta_Z_WZ = cos(Z_WZframe.theta());
      
      double CosTheta_WLep_Wrest = cos(Wlep_Wframe.angle(Wboson));
      double CosTheta_WLep_Wrest_wWZrest = cos(Wlep_Wframe_WZ.angle(W_WZframe));
      double CosTheta_ZLepM_Zrest = cos(ZlepM_Zframe.angle(Z_WZframe));
      double CosTheta_ZLepM_Zrest_wWZrest = cos(ZlepM_Zframe_WZ.angle(Z_WZframe));
      
      double DY_WZ = Zboson.rapidity() - Wboson.rapidity();
      double DY_3Z = Zboson.rapidity() - Wlepton.rapidity();
      double DY_3N = ZleptonM.rapidity() - Wlepton.rapidity();

      double DeltaPhiLepWLepZ = Phi_mpi_to_pi(ZleptonM.phi() - Wlepton.phi());
      double DeltaPhiLepWLepZWZframe = -99.0;
      if( !(ZleptonM.pt() < 1e-10 || Wlepton.pt() < 1e-10) ){
	DeltaPhiLepWLepZWZframe = Phi_mpi_to_pi(ZlepM_WZframe.phi() - Wlep_WZframe.phi());
      }

      _passCuts[ps_All] = true;
      _eventsPassCuts[ps_All] += 1;

      fillWithOverUnderflow(h_Cut0_WZ_pT,pT_WZ);
      fillWithOverUnderflow(h_Cut0_pT_W,pT_W);
      fillWithOverUnderflow(h_Cut0_pT_Z,pT_Z);
      fillWithOverUnderflow(h_Cut0_M_WZ,WZ.mass());
      fillWithOverUnderflow(h_Cut0_M_Z,Zboson.mass());
      fillWithOverUnderflow(h_Cut0_M_3l,(Zlepton1+Zlepton2+Wlepton).mass());
      fillWithOverUnderflow(h_Cut0_mT_WZ,mTWZ);
      fillWithOverUnderflow(h_Cut0_mT_W,mTW);
      fillWithOverUnderflow(h_Cut0_MET,MET);
      h_Cut0_R21->fill(R21);
      fillWithOverUnderflow(h_Cut0_DY_WZ,DY_WZ);
      fillWithOverUnderflow(h_Cut0_DY_3Z,DY_3Z);
      fillWithOverUnderflow(h_Cut0_DY_3N,DY_3N);
      h_Cut0_CosTheta_W_Lab->fill(CosTheta_W_Lab);
      h_Cut0_CosTheta_Z_Lab->fill(CosTheta_Z_Lab);
      h_Cut0_CosTheta_W_WZ->fill(CosTheta_W_WZ);
      h_Cut0_CosTheta_Z_WZ->fill(CosTheta_Z_WZ);
      h_Cut0_CosTheta_WLep_Wrest->fill(CosTheta_WLep_Wrest);
      h_Cut0_CosTheta_WLep_Wrest_wWZrest->fill(CosTheta_WLep_Wrest_wWZrest);
      h_Cut0_CosTheta_ZLepM_Zrest->fill(CosTheta_ZLepM_Zrest);
      h_Cut0_CosTheta_ZLepM_Zrest_wWZrest->fill(CosTheta_ZLepM_Zrest_wWZrest);
      fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZ,DeltaPhiLepWLepZ);
      fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZWZframe,DeltaPhiLepWLepZWZframe);

      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut0_CosTheta_W_WZ_WLZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut0_DY_3Z_WLZL,DY_3Z);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZ_WLZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZWZframe_WLZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut0_CosTheta_W_WZ_WLZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut0_DY_3Z_WLZH,DY_3Z);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZ_WLZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZWZframe_WLZH,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut0_CosTheta_W_WZ_WHZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut0_DY_3Z_WHZL,DY_3Z);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZ_WHZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZWZframe_WHZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut0_CosTheta_W_WZ_WHZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut0_DY_3Z_WHZH,DY_3Z);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZ_WHZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut0_DeltaPhiLepWLepZWZframe_WHZH,DeltaPhiLepWLepZWZframe);
      }

      fillWithOverUnderflow(h_Cut0_all_Lep_pT,Zlepton1.pt());
      fillWithOverUnderflow(h_Cut0_all_Lep_pT,Zlepton2.pt());
      fillWithOverUnderflow(h_Cut0_all_Lep_pT,Wlepton.pt());
      fillWithOverUnderflow(h_Cut0_all_Lep_Eta,Zlepton1.eta());
      fillWithOverUnderflow(h_Cut0_all_Lep_Eta,Zlepton2.eta());
      fillWithOverUnderflow(h_Cut0_all_Lep_Eta,Wlepton.eta());
      fillWithOverUnderflow(h_Cut0_all_Lep_Phi,Zlepton1.phi()-M_PI);
      fillWithOverUnderflow(h_Cut0_all_Lep_Phi,Zlepton2.phi()-M_PI);
      fillWithOverUnderflow(h_Cut0_all_Lep_Phi,Wlepton.phi()-M_PI);

      if (Wlepton.pT() <= 20*GeV || Zlepton1.pT() <= 15*GeV || Zlepton2.pT() <= 15*GeV)                  vetoEvent;
      if (fabs(Wlepton.eta()) >= 2.5 || fabs(Zlepton1.eta()) >= 2.5 || fabs(Zlepton2.eta()) >= 2.5)            vetoEvent;
      if (fabs(Zboson.mass()/GeV - MZ_PDG) >= 10.) vetoEvent;
      if (mTW <= 30*GeV)                     vetoEvent;
      if (deltaR(Zlepton1, Zlepton2) <= 0.2)        vetoEvent;
      if (deltaR(Zlepton1, Wlepton)  <= 0.3)        vetoEvent;
      if (deltaR(Zlepton2, Wlepton)  <= 0.3)        vetoEvent;

      // Selection cuts 
      
      // Cut 1: Lepton Cut
      _passCuts[ps_Lep] = true; 
      _eventsPassCuts[ps_Lep] += 1;
       
      fillWithOverUnderflow(h_Cut1_WZ_pT,pT_WZ);
      fillWithOverUnderflow(h_Cut1_pT_W,pT_W);
      fillWithOverUnderflow(h_Cut1_pT_Z,pT_Z);
      fillWithOverUnderflow(h_Cut1_M_WZ,WZ.mass());
      fillWithOverUnderflow(h_Cut1_M_Z,Zboson.mass());
      fillWithOverUnderflow(h_Cut1_M_3l,(Zlepton1+Zlepton2+Wlepton).mass());
      fillWithOverUnderflow(h_Cut1_mT_WZ,mTWZ);
      fillWithOverUnderflow(h_Cut1_mT_W,mTW);
      fillWithOverUnderflow(h_Cut1_MET,MET);
      h_Cut1_R21->fill(R21);
      fillWithOverUnderflow(h_Cut1_DY_WZ,DY_WZ);
      fillWithOverUnderflow(h_Cut1_DY_3Z,DY_3Z);
      fillWithOverUnderflow(h_Cut1_DY_3N,DY_3N);
      h_Cut1_CosTheta_W_Lab->fill(CosTheta_W_Lab);
      h_Cut1_CosTheta_Z_Lab->fill(CosTheta_Z_Lab);
      h_Cut1_CosTheta_W_WZ->fill(CosTheta_W_WZ);
      h_Cut1_CosTheta_Z_WZ->fill(CosTheta_Z_WZ);
      h_Cut1_CosTheta_WLep_Wrest->fill(CosTheta_WLep_Wrest);
      h_Cut1_CosTheta_WLep_Wrest_wWZrest->fill(CosTheta_WLep_Wrest_wWZrest);
      h_Cut1_CosTheta_ZLepM_Zrest->fill(CosTheta_ZLepM_Zrest);
      h_Cut1_CosTheta_ZLepM_Zrest_wWZrest->fill(CosTheta_ZLepM_Zrest_wWZrest);
      fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZ,DeltaPhiLepWLepZ);
      fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZWZframe,DeltaPhiLepWLepZWZframe);

      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut1_CosTheta_W_WZ_WLZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut1_DY_3Z_WLZL,DY_3Z);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZ_WLZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZWZframe_WLZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut1_CosTheta_W_WZ_WLZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut1_DY_3Z_WLZH,DY_3Z);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZ_WLZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZWZframe_WLZH,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut1_CosTheta_W_WZ_WHZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut1_DY_3Z_WHZL,DY_3Z);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZ_WHZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZWZframe_WHZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut1_CosTheta_W_WZ_WHZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut1_DY_3Z_WHZH,DY_3Z);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZ_WHZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut1_DeltaPhiLepWLepZWZframe_WHZH,DeltaPhiLepWLepZWZframe);
      }

      fillWithOverUnderflow(h_Cut1_all_Lep_pT,Zlepton1.pt());
      fillWithOverUnderflow(h_Cut1_all_Lep_pT,Zlepton2.pt());
      fillWithOverUnderflow(h_Cut1_all_Lep_pT,Wlepton.pt());
      fillWithOverUnderflow(h_Cut1_all_Lep_Eta,Zlepton1.eta());
      fillWithOverUnderflow(h_Cut1_all_Lep_Eta,Zlepton2.eta());
      fillWithOverUnderflow(h_Cut1_all_Lep_Eta,Wlepton.eta());
      fillWithOverUnderflow(h_Cut1_all_Lep_Phi,Zlepton1.phi()-M_PI);
      fillWithOverUnderflow(h_Cut1_all_Lep_Phi,Zlepton2.phi()-M_PI);
      fillWithOverUnderflow(h_Cut1_all_Lep_Phi,Wlepton.phi()-M_PI);
      
      //Cut 2: pT(Z)>200 GeV
      if(pT_Z <= 200) vetoEvent;
      
      _passCuts[ps_ptZ] = true; 
      _eventsPassCuts[ps_ptZ] += 1;

      fillWithOverUnderflow(h_Cut2_WZ_pT,pT_WZ);
      fillWithOverUnderflow(h_Cut2_pT_W,pT_W);
      fillWithOverUnderflow(h_Cut2_pT_Z,pT_Z);
      fillWithOverUnderflow(h_Cut2_M_WZ,WZ.mass());
      fillWithOverUnderflow(h_Cut2_M_Z,Zboson.mass());
      fillWithOverUnderflow(h_Cut2_M_3l,(Zlepton1+Zlepton2+Wlepton).mass());
      fillWithOverUnderflow(h_Cut2_mT_WZ,mTWZ);
      fillWithOverUnderflow(h_Cut2_mT_W,mTW);
      fillWithOverUnderflow(h_Cut2_MET,MET);
      h_Cut2_R21->fill(R21);
      fillWithOverUnderflow(h_Cut2_DY_WZ,DY_WZ);
      fillWithOverUnderflow(h_Cut2_DY_3Z,DY_3Z);
      fillWithOverUnderflow(h_Cut2_DY_3N,DY_3N);
      h_Cut2_CosTheta_W_Lab->fill(CosTheta_W_Lab);
      h_Cut2_CosTheta_Z_Lab->fill(CosTheta_Z_Lab);
      h_Cut2_CosTheta_W_WZ->fill(CosTheta_W_WZ);
      h_Cut2_CosTheta_Z_WZ->fill(CosTheta_Z_WZ);
      h_Cut2_CosTheta_WLep_Wrest->fill(CosTheta_WLep_Wrest);
      h_Cut2_CosTheta_WLep_Wrest_wWZrest->fill(CosTheta_WLep_Wrest_wWZrest);
      h_Cut2_CosTheta_ZLepM_Zrest->fill(CosTheta_ZLepM_Zrest);
      h_Cut2_CosTheta_ZLepM_Zrest_wWZrest->fill(CosTheta_ZLepM_Zrest_wWZrest);
      fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZ,DeltaPhiLepWLepZ);
      fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZWZframe,DeltaPhiLepWLepZWZframe);

      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut2_CosTheta_W_WZ_WLZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut2_DY_3Z_WLZL,DY_3Z);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZ_WLZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZWZframe_WLZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut2_CosTheta_W_WZ_WLZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut2_DY_3Z_WLZH,DY_3Z);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZ_WLZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZWZframe_WLZH,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut2_CosTheta_W_WZ_WHZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut2_DY_3Z_WHZL,DY_3Z);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZ_WHZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZWZframe_WHZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut2_CosTheta_W_WZ_WHZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut2_DY_3Z_WHZH,DY_3Z);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZ_WHZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut2_DeltaPhiLepWLepZWZframe_WHZH,DeltaPhiLepWLepZWZframe);
      }

      fillWithOverUnderflow(h_Cut2_all_Lep_pT,Zlepton1.pt());
      fillWithOverUnderflow(h_Cut2_all_Lep_pT,Zlepton2.pt());
      fillWithOverUnderflow(h_Cut2_all_Lep_pT,Wlepton.pt());
      fillWithOverUnderflow(h_Cut2_all_Lep_Eta,Zlepton1.eta());
      fillWithOverUnderflow(h_Cut2_all_Lep_Eta,Zlepton2.eta());
      fillWithOverUnderflow(h_Cut2_all_Lep_Eta,Wlepton.eta());
      fillWithOverUnderflow(h_Cut2_all_Lep_Phi,Zlepton1.phi()-M_PI);
      fillWithOverUnderflow(h_Cut2_all_Lep_Phi,Zlepton2.phi()-M_PI);
      fillWithOverUnderflow(h_Cut2_all_Lep_Phi,Wlepton.phi()-M_PI);
      
      //Cut 3: R21 > 0.8 
      if(R21 <= 0.8) vetoEvent;
      
      _passCuts[ps_R21] = true; 
      _eventsPassCuts[ps_R21] += 1;
      
      fillWithOverUnderflow(h_Cut3_WZ_pT,pT_WZ);
      fillWithOverUnderflow(h_Cut3_pT_W,pT_W);
      fillWithOverUnderflow(h_Cut3_pT_Z,pT_Z);
      fillWithOverUnderflow(h_Cut3_M_WZ,WZ.mass());
      fillWithOverUnderflow(h_Cut3_M_Z,Zboson.mass());
      fillWithOverUnderflow(h_Cut3_M_3l,(Zlepton1+Zlepton2+Wlepton).mass());
      fillWithOverUnderflow(h_Cut3_mT_WZ,mTWZ);
      fillWithOverUnderflow(h_Cut3_mT_W,mTW);
      fillWithOverUnderflow(h_Cut3_MET,MET);
      h_Cut3_R21->fill(R21);
      fillWithOverUnderflow(h_Cut3_DY_WZ,DY_WZ);
      fillWithOverUnderflow(h_Cut3_DY_3Z,DY_3Z);
      fillWithOverUnderflow(h_Cut3_DY_3N,DY_3N);
      h_Cut3_CosTheta_W_Lab->fill(CosTheta_W_Lab);
      h_Cut3_CosTheta_Z_Lab->fill(CosTheta_Z_Lab);
      h_Cut3_CosTheta_W_WZ->fill(CosTheta_W_WZ);
      h_Cut3_CosTheta_Z_WZ->fill(CosTheta_Z_WZ);
      h_Cut3_CosTheta_WLep_Wrest->fill(CosTheta_WLep_Wrest);
      h_Cut3_CosTheta_WLep_Wrest_wWZrest->fill(CosTheta_WLep_Wrest_wWZrest);
      h_Cut3_CosTheta_ZLepM_Zrest->fill(CosTheta_ZLepM_Zrest);
      h_Cut3_CosTheta_ZLepM_Zrest_wWZrest->fill(CosTheta_ZLepM_Zrest_wWZrest);
      fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZ,DeltaPhiLepWLepZ);
      fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZWZframe,DeltaPhiLepWLepZWZframe);

      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut3_CosTheta_W_WZ_WLZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut3_DY_3Z_WLZL,DY_3Z);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZ_WLZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZWZframe_WLZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) < 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut3_CosTheta_W_WZ_WLZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut3_DY_3Z_WLZH,DY_3Z);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZ_WLZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZWZframe_WLZH,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) < 0.5 ){
	h_Cut3_CosTheta_W_WZ_WHZL->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut3_DY_3Z_WHZL,DY_3Z);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZ_WHZL,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZWZframe_WHZL,DeltaPhiLepWLepZWZframe);
      }
      if( fabs(CosTheta_WLep_Wrest_wWZrest) > 0.5 && fabs(CosTheta_ZLepM_Zrest_wWZrest) > 0.5 ){
	h_Cut3_CosTheta_W_WZ_WHZH->fill(CosTheta_W_WZ);
	fillWithOverUnderflow(h_Cut3_DY_3Z_WHZH,DY_3Z);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZ_WHZH,DeltaPhiLepWLepZ);
	fillWithOverUnderflow(h_Cut3_DeltaPhiLepWLepZWZframe_WHZH,DeltaPhiLepWLepZWZframe);
      }

      fillWithOverUnderflow(h_Cut3_all_Lep_pT,Zlepton1.pt());
      fillWithOverUnderflow(h_Cut3_all_Lep_pT,Zlepton2.pt());
      fillWithOverUnderflow(h_Cut3_all_Lep_pT,Wlepton.pt());
      fillWithOverUnderflow(h_Cut3_all_Lep_Eta,Zlepton1.eta());
      fillWithOverUnderflow(h_Cut3_all_Lep_Eta,Zlepton2.eta());
      fillWithOverUnderflow(h_Cut3_all_Lep_Eta,Wlepton.eta());
      fillWithOverUnderflow(h_Cut3_all_Lep_Phi,Zlepton1.phi()-M_PI);
      fillWithOverUnderflow(h_Cut3_all_Lep_Phi,Zlepton2.phi()-M_PI);
      fillWithOverUnderflow(h_Cut3_all_Lep_Phi,Wlepton.phi()-M_PI);
      
    }

    void fillWithOverUnderflow(const Histo1DPtr& _h, const double value){
      double width = _h->bin(_h->numBinsX()-1).xWidth();
      //MSG_INFO("Width of the bin: "<<width);
      double overflow = _h->bin(_h->numBinsX()-1).xMax();
      //MSG_INFO("Overflow of the bin: "<<overflow);
      double underflow = _h->bin(0).xMin();
      if (value >= underflow && value < overflow) _h->fill(value);
      else if(value<underflow) _h->fill(underflow+(width/2));
      else if(value >= overflow) _h->fill(overflow-(width/2));

    }

    void finalize() {

      // Print summary info
      const double xs_pb(crossSection() / picobarn);
      const double xs_fb(crossSection() / femtobarn);
      const double sumw(sumW());
      const double sf_pb(xs_pb / sumw);
      const double sf_fb(xs_fb / sumw);

      const float totalBR= 4*0.1086*0.033658; // W and Z leptonic branching fractions

      m_strOutput << "xs_pb: " << xs_pb << std::endl;
      m_strOutput << "xs_fb: " << xs_fb << std::endl;
      m_strOutput << "sumw: " << sumw << std::endl;
      m_strOutput << "sf_pb: " << sf_pb << std::endl;
      m_strOutput << "sf_fb: " << sf_fb << std::endl;
      
      m_strOutput << "ps_PreFilter: " << _eventsPassCuts[ps_PreFilter] << std::endl;
      m_strOutput << "ps_All: " << _eventsPassCuts[ps_All] << std::endl;
      m_strOutput << "ps_Lep: " << _eventsPassCuts[ps_Lep] << std::endl;
      m_strOutput << "ps_ptZ: "<< _eventsPassCuts[ps_ptZ] << std::endl;
      m_strOutput << "ps_R21: "<< _eventsPassCuts[ps_R21] << std::endl;
      
      m_strOutput << std::endl;

      m_strOutput << "Filters used: " << std::endl;

      m_strOutput << "All lepton pT > 15 GeV" << std::endl;
      m_strOutput << "All Lepton absolute eta < 2.5"<< std::endl;
      m_strOutput << "if (fabs(Zboson.mass()/GeV - MZ_PDG) >= 10.) vetoEvent;"<< std::endl;
      m_strOutput << "if (mTW <= 30*GeV)                     vetoEvent;"<< std::endl;
      m_strOutput << "if (deltaR(Zlepton1, Zlepton2) < 0.2)        vetoEvent;"<< std::endl;
      m_strOutput << "if (deltaR(Zlepton1, Wlepton)  < 0.3)        vetoEvent;"<< std::endl;
      m_strOutput << "if (deltaR(Zlepton2, Wlepton)  < 0.3)        vetoEvent;"<< std::endl;

      m_strOutput.close();

      scale(h_Cut0_WZ_pT, sf_pb);
      scale(h_Cut0_pT_W, sf_pb);
      scale(h_Cut0_pT_Z, sf_pb);
      scale(h_Cut0_M_WZ, sf_pb);
      scale(h_Cut0_M_Z, sf_pb);
      scale(h_Cut0_M_3l, sf_pb);
      scale(h_Cut0_mT_WZ, sf_pb);
      scale(h_Cut0_mT_W, sf_pb);
      scale(h_Cut0_MET, sf_pb);
      scale(h_Cut0_R21, sf_pb);
      scale(h_Cut0_DY_WZ, sf_pb);
      scale(h_Cut0_DY_3Z, sf_pb);
      scale(h_Cut0_DY_3Z_WLZL, sf_pb);
      scale(h_Cut0_DY_3Z_WLZH, sf_pb);
      scale(h_Cut0_DY_3Z_WHZL, sf_pb);
      scale(h_Cut0_DY_3Z_WHZH, sf_pb);
      scale(h_Cut0_DY_3N, sf_pb);
      scale(h_Cut0_CosTheta_W_Lab, sf_pb);
      scale(h_Cut0_CosTheta_Z_Lab, sf_pb);
      scale(h_Cut0_CosTheta_W_WZ, sf_pb);
      scale(h_Cut0_CosTheta_W_WZ_WLZL, sf_pb);
      scale(h_Cut0_CosTheta_W_WZ_WLZH, sf_pb);
      scale(h_Cut0_CosTheta_W_WZ_WHZL, sf_pb);
      scale(h_Cut0_CosTheta_W_WZ_WHZH, sf_pb);
      scale(h_Cut0_CosTheta_Z_WZ, sf_pb);
      scale(h_Cut0_CosTheta_WLep_Wrest, sf_pb);
      scale(h_Cut0_CosTheta_WLep_Wrest_wWZrest, sf_pb);
      scale(h_Cut0_CosTheta_ZLepM_Zrest, sf_pb);
      scale(h_Cut0_CosTheta_ZLepM_Zrest_wWZrest, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZ, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZ_WLZL, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZ_WLZH, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZ_WHZL, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZ_WHZH, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZWZframe, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZWZframe_WLZL, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZWZframe_WLZH, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZWZframe_WHZL, sf_pb);
      scale(h_Cut0_DeltaPhiLepWLepZWZframe_WHZH, sf_pb);
      scale(h_Cut0_all_Lep_pT, sf_pb);
      scale(h_Cut0_all_Lep_Eta, sf_pb);
      scale(h_Cut0_all_Lep_Phi, sf_pb);
      
      scale(h_Cut1_WZ_pT, sf_pb);
      scale(h_Cut1_pT_W, sf_pb);
      scale(h_Cut1_pT_Z, sf_pb);
      scale(h_Cut1_M_WZ, sf_pb);
      scale(h_Cut1_M_Z, sf_pb);
      scale(h_Cut1_M_3l, sf_pb);
      scale(h_Cut1_mT_WZ, sf_pb);
      scale(h_Cut1_mT_W, sf_pb);
      scale(h_Cut1_MET, sf_pb);
      scale(h_Cut1_R21, sf_pb);
      scale(h_Cut1_DY_WZ, sf_pb);
      scale(h_Cut1_DY_3Z, sf_pb);
      scale(h_Cut1_DY_3Z_WLZL, sf_pb);
      scale(h_Cut1_DY_3Z_WLZH, sf_pb);
      scale(h_Cut1_DY_3Z_WHZL, sf_pb);
      scale(h_Cut1_DY_3Z_WHZH, sf_pb);
      scale(h_Cut1_DY_3N, sf_pb);
      scale(h_Cut1_CosTheta_W_Lab, sf_pb);
      scale(h_Cut1_CosTheta_Z_Lab, sf_pb);
      scale(h_Cut1_CosTheta_W_WZ, sf_pb);
      scale(h_Cut1_CosTheta_W_WZ_WLZL, sf_pb);
      scale(h_Cut1_CosTheta_W_WZ_WLZH, sf_pb);
      scale(h_Cut1_CosTheta_W_WZ_WHZL, sf_pb);
      scale(h_Cut1_CosTheta_W_WZ_WHZH, sf_pb);
      scale(h_Cut1_CosTheta_Z_WZ, sf_pb);
      scale(h_Cut1_CosTheta_WLep_Wrest, sf_pb);
      scale(h_Cut1_CosTheta_WLep_Wrest_wWZrest, sf_pb);
      scale(h_Cut1_CosTheta_ZLepM_Zrest, sf_pb);
      scale(h_Cut1_CosTheta_ZLepM_Zrest_wWZrest, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZ, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZ_WLZL, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZ_WLZH, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZ_WHZL, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZ_WHZH, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZWZframe, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZWZframe_WLZL, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZWZframe_WLZH, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZWZframe_WHZL, sf_pb);
      scale(h_Cut1_DeltaPhiLepWLepZWZframe_WHZH, sf_pb);
      scale(h_Cut1_all_Lep_pT, sf_pb);
      scale(h_Cut1_all_Lep_Eta, sf_pb);
      scale(h_Cut1_all_Lep_Phi, sf_pb);
      
      scale(h_Cut2_WZ_pT, sf_pb);
      scale(h_Cut2_pT_W, sf_pb);
      scale(h_Cut2_pT_Z, sf_pb);
      scale(h_Cut2_M_WZ, sf_pb);
      scale(h_Cut2_M_Z, sf_pb);
      scale(h_Cut2_M_3l, sf_pb);
      scale(h_Cut2_mT_WZ, sf_pb);
      scale(h_Cut2_mT_W, sf_pb);
      scale(h_Cut2_MET, sf_pb);
      scale(h_Cut2_R21, sf_pb);
      scale(h_Cut2_DY_WZ, sf_pb);
      scale(h_Cut2_DY_3Z, sf_pb);
      scale(h_Cut2_DY_3Z_WLZL, sf_pb);
      scale(h_Cut2_DY_3Z_WLZH, sf_pb);
      scale(h_Cut2_DY_3Z_WHZL, sf_pb);
      scale(h_Cut2_DY_3Z_WHZH, sf_pb);
      scale(h_Cut2_DY_3N, sf_pb);
      scale(h_Cut2_CosTheta_W_Lab, sf_pb);
      scale(h_Cut2_CosTheta_Z_Lab, sf_pb);
      scale(h_Cut2_CosTheta_W_WZ, sf_pb);
      scale(h_Cut2_CosTheta_W_WZ_WLZL, sf_pb);
      scale(h_Cut2_CosTheta_W_WZ_WLZH, sf_pb);
      scale(h_Cut2_CosTheta_W_WZ_WHZL, sf_pb);
      scale(h_Cut2_CosTheta_W_WZ_WHZH, sf_pb);
      scale(h_Cut2_CosTheta_Z_WZ, sf_pb);
      scale(h_Cut2_CosTheta_WLep_Wrest, sf_pb);
      scale(h_Cut2_CosTheta_WLep_Wrest_wWZrest, sf_pb);
      scale(h_Cut2_CosTheta_ZLepM_Zrest, sf_pb);
      scale(h_Cut2_CosTheta_ZLepM_Zrest_wWZrest, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZ, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZ_WLZL, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZ_WLZH, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZ_WHZL, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZ_WHZH, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZWZframe, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZWZframe_WLZL, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZWZframe_WLZH, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZWZframe_WHZL, sf_pb);
      scale(h_Cut2_DeltaPhiLepWLepZWZframe_WHZH, sf_pb);
      scale(h_Cut2_all_Lep_pT, sf_pb);
      scale(h_Cut2_all_Lep_Eta, sf_pb);
      scale(h_Cut2_all_Lep_Phi, sf_pb);
      
      scale(h_Cut3_WZ_pT, sf_pb);
      scale(h_Cut3_pT_W, sf_pb);
      scale(h_Cut3_pT_Z, sf_pb);
      scale(h_Cut3_M_WZ, sf_pb);
      scale(h_Cut3_M_Z, sf_pb);
      scale(h_Cut3_M_3l, sf_pb);
      scale(h_Cut3_mT_WZ, sf_pb);
      scale(h_Cut3_mT_W, sf_pb);
      scale(h_Cut3_MET, sf_pb);
      scale(h_Cut3_R21, sf_pb);
      scale(h_Cut3_DY_WZ, sf_pb);
      scale(h_Cut3_DY_3Z, sf_pb);
      scale(h_Cut3_DY_3Z_WLZL, sf_pb);
      scale(h_Cut3_DY_3Z_WLZH, sf_pb);
      scale(h_Cut3_DY_3Z_WHZL, sf_pb);
      scale(h_Cut3_DY_3Z_WHZH, sf_pb);
      scale(h_Cut3_DY_3N, sf_pb);
      scale(h_Cut3_CosTheta_W_Lab, sf_pb);
      scale(h_Cut3_CosTheta_Z_Lab, sf_pb);
      scale(h_Cut3_CosTheta_W_WZ, sf_pb);
      scale(h_Cut3_CosTheta_W_WZ_WLZL, sf_pb);
      scale(h_Cut3_CosTheta_W_WZ_WLZH, sf_pb);
      scale(h_Cut3_CosTheta_W_WZ_WHZL, sf_pb);
      scale(h_Cut3_CosTheta_W_WZ_WHZH, sf_pb);
      scale(h_Cut3_CosTheta_Z_WZ, sf_pb);
      scale(h_Cut3_CosTheta_WLep_Wrest, sf_pb);
      scale(h_Cut3_CosTheta_WLep_Wrest_wWZrest, sf_pb);
      scale(h_Cut3_CosTheta_ZLepM_Zrest, sf_pb);
      scale(h_Cut3_CosTheta_ZLepM_Zrest_wWZrest, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZ, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZ_WLZL, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZ_WLZH, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZ_WHZL, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZ_WHZH, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZWZframe, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZWZframe_WLZL, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZWZframe_WLZH, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZWZframe_WHZL, sf_pb);
      scale(h_Cut3_DeltaPhiLepWLepZWZframe_WHZH, sf_pb);
      scale(h_Cut3_all_Lep_pT, sf_pb);
      scale(h_Cut3_all_Lep_Eta, sf_pb);
      scale(h_Cut3_all_Lep_Phi, sf_pb);

    }

    //@}


  private:


    /// @name Histograms
    //@{
    
    Histo1DPtr h_Cut0_WZ_pT;
    Histo1DPtr h_Cut0_pT_W;
    Histo1DPtr h_Cut0_pT_Z;
    Histo1DPtr h_Cut0_M_WZ;
    Histo1DPtr h_Cut0_M_Z;
    Histo1DPtr h_Cut0_M_3l;
    Histo1DPtr h_Cut0_mT_WZ;
    Histo1DPtr h_Cut0_mT_W;
    Histo1DPtr h_Cut0_MET;
    Histo1DPtr h_Cut0_R21;
    Histo1DPtr h_Cut0_DY_WZ;
    Histo1DPtr h_Cut0_DY_3Z;
    Histo1DPtr h_Cut0_DY_3Z_WLZL;
    Histo1DPtr h_Cut0_DY_3Z_WLZH;
    Histo1DPtr h_Cut0_DY_3Z_WHZL;
    Histo1DPtr h_Cut0_DY_3Z_WHZH;
    Histo1DPtr h_Cut0_DY_3N;
    Histo1DPtr h_Cut0_CosTheta_W_Lab;
    Histo1DPtr h_Cut0_CosTheta_Z_Lab;
    Histo1DPtr h_Cut0_CosTheta_W_WZ;
    Histo1DPtr h_Cut0_CosTheta_W_WZ_WLZL;
    Histo1DPtr h_Cut0_CosTheta_W_WZ_WLZH;
    Histo1DPtr h_Cut0_CosTheta_W_WZ_WHZL;
    Histo1DPtr h_Cut0_CosTheta_W_WZ_WHZH;
    Histo1DPtr h_Cut0_CosTheta_Z_WZ;
    Histo1DPtr h_Cut0_CosTheta_WLep_Wrest;
    Histo1DPtr h_Cut0_CosTheta_WLep_Wrest_wWZrest;
    Histo1DPtr h_Cut0_CosTheta_ZLepM_Zrest;
    Histo1DPtr h_Cut0_CosTheta_ZLepM_Zrest_wWZrest;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZ;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZ_WLZL;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZ_WLZH;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZ_WHZL;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZ_WHZH;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZWZframe;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZWZframe_WLZL;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZWZframe_WLZH;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZWZframe_WHZL;
    Histo1DPtr h_Cut0_DeltaPhiLepWLepZWZframe_WHZH;
    Histo1DPtr h_Cut0_all_Lep_pT;
    Histo1DPtr h_Cut0_all_Lep_Eta;
    Histo1DPtr h_Cut0_all_Lep_Phi;

    Histo1DPtr h_Cut1_WZ_pT;
    Histo1DPtr h_Cut1_pT_W;
    Histo1DPtr h_Cut1_pT_Z;
    Histo1DPtr h_Cut1_M_WZ;
    Histo1DPtr h_Cut1_M_Z;
    Histo1DPtr h_Cut1_M_3l;
    Histo1DPtr h_Cut1_mT_WZ;
    Histo1DPtr h_Cut1_mT_W;
    Histo1DPtr h_Cut1_MET;
    Histo1DPtr h_Cut1_R21;
    Histo1DPtr h_Cut1_DY_WZ;
    Histo1DPtr h_Cut1_DY_3Z;
    Histo1DPtr h_Cut1_DY_3Z_WLZL;
    Histo1DPtr h_Cut1_DY_3Z_WLZH;
    Histo1DPtr h_Cut1_DY_3Z_WHZL;
    Histo1DPtr h_Cut1_DY_3Z_WHZH;
    Histo1DPtr h_Cut1_DY_3N;
    Histo1DPtr h_Cut1_CosTheta_W_Lab;
    Histo1DPtr h_Cut1_CosTheta_Z_Lab;
    Histo1DPtr h_Cut1_CosTheta_W_WZ;
    Histo1DPtr h_Cut1_CosTheta_W_WZ_WLZL;
    Histo1DPtr h_Cut1_CosTheta_W_WZ_WLZH;
    Histo1DPtr h_Cut1_CosTheta_W_WZ_WHZL;
    Histo1DPtr h_Cut1_CosTheta_W_WZ_WHZH;
    Histo1DPtr h_Cut1_CosTheta_Z_WZ;
    Histo1DPtr h_Cut1_CosTheta_WLep_Wrest;
    Histo1DPtr h_Cut1_CosTheta_WLep_Wrest_wWZrest;
    Histo1DPtr h_Cut1_CosTheta_ZLepM_Zrest;
    Histo1DPtr h_Cut1_CosTheta_ZLepM_Zrest_wWZrest;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZ;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZ_WLZL;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZ_WLZH;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZ_WHZL;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZ_WHZH;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZWZframe;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZWZframe_WLZL;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZWZframe_WLZH;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZWZframe_WHZL;
    Histo1DPtr h_Cut1_DeltaPhiLepWLepZWZframe_WHZH;
    Histo1DPtr h_Cut1_all_Lep_pT;
    Histo1DPtr h_Cut1_all_Lep_Eta;
    Histo1DPtr h_Cut1_all_Lep_Phi;
    
    Histo1DPtr h_Cut2_WZ_pT;
    Histo1DPtr h_Cut2_pT_W;
    Histo1DPtr h_Cut2_pT_Z;
    Histo1DPtr h_Cut2_M_WZ;
    Histo1DPtr h_Cut2_M_Z;
    Histo1DPtr h_Cut2_M_3l;
    Histo1DPtr h_Cut2_mT_WZ;
    Histo1DPtr h_Cut2_mT_W;
    Histo1DPtr h_Cut2_MET;
    Histo1DPtr h_Cut2_R21;
    Histo1DPtr h_Cut2_DY_WZ;
    Histo1DPtr h_Cut2_DY_3Z;
    Histo1DPtr h_Cut2_DY_3Z_WLZL;
    Histo1DPtr h_Cut2_DY_3Z_WLZH;
    Histo1DPtr h_Cut2_DY_3Z_WHZL;
    Histo1DPtr h_Cut2_DY_3Z_WHZH;
    Histo1DPtr h_Cut2_DY_3N;
    Histo1DPtr h_Cut2_CosTheta_W_Lab;
    Histo1DPtr h_Cut2_CosTheta_Z_Lab;
    Histo1DPtr h_Cut2_CosTheta_W_WZ;
    Histo1DPtr h_Cut2_CosTheta_W_WZ_WLZL;
    Histo1DPtr h_Cut2_CosTheta_W_WZ_WLZH;
    Histo1DPtr h_Cut2_CosTheta_W_WZ_WHZL;
    Histo1DPtr h_Cut2_CosTheta_W_WZ_WHZH;
    Histo1DPtr h_Cut2_CosTheta_Z_WZ;
    Histo1DPtr h_Cut2_CosTheta_WLep_Wrest;
    Histo1DPtr h_Cut2_CosTheta_WLep_Wrest_wWZrest;
    Histo1DPtr h_Cut2_CosTheta_ZLepM_Zrest;
    Histo1DPtr h_Cut2_CosTheta_ZLepM_Zrest_wWZrest;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZ;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZ_WLZL;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZ_WLZH;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZ_WHZL;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZ_WHZH;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZWZframe;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZWZframe_WLZL;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZWZframe_WLZH;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZWZframe_WHZL;
    Histo1DPtr h_Cut2_DeltaPhiLepWLepZWZframe_WHZH;
    Histo1DPtr h_Cut2_all_Lep_pT;
    Histo1DPtr h_Cut2_all_Lep_Eta;
    Histo1DPtr h_Cut2_all_Lep_Phi;

    Histo1DPtr h_Cut3_WZ_pT;
    Histo1DPtr h_Cut3_pT_W;
    Histo1DPtr h_Cut3_pT_Z;
    Histo1DPtr h_Cut3_M_WZ;
    Histo1DPtr h_Cut3_M_Z;
    Histo1DPtr h_Cut3_M_3l;
    Histo1DPtr h_Cut3_mT_WZ;
    Histo1DPtr h_Cut3_mT_W;
    Histo1DPtr h_Cut3_MET;
    Histo1DPtr h_Cut3_R21;
    Histo1DPtr h_Cut3_DY_WZ;
    Histo1DPtr h_Cut3_DY_3Z;
    Histo1DPtr h_Cut3_DY_3Z_WLZL;
    Histo1DPtr h_Cut3_DY_3Z_WLZH;
    Histo1DPtr h_Cut3_DY_3Z_WHZL;
    Histo1DPtr h_Cut3_DY_3Z_WHZH;
    Histo1DPtr h_Cut3_DY_3N;
    Histo1DPtr h_Cut3_CosTheta_W_Lab;
    Histo1DPtr h_Cut3_CosTheta_Z_Lab;
    Histo1DPtr h_Cut3_CosTheta_W_WZ;
    Histo1DPtr h_Cut3_CosTheta_W_WZ_WLZL;
    Histo1DPtr h_Cut3_CosTheta_W_WZ_WLZH;
    Histo1DPtr h_Cut3_CosTheta_W_WZ_WHZL;
    Histo1DPtr h_Cut3_CosTheta_W_WZ_WHZH;
    Histo1DPtr h_Cut3_CosTheta_Z_WZ;
    Histo1DPtr h_Cut3_CosTheta_WLep_Wrest;
    Histo1DPtr h_Cut3_CosTheta_WLep_Wrest_wWZrest;
    Histo1DPtr h_Cut3_CosTheta_ZLepM_Zrest;
    Histo1DPtr h_Cut3_CosTheta_ZLepM_Zrest_wWZrest;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZ;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZ_WLZL;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZ_WLZH;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZ_WHZL;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZ_WHZH;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZWZframe;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZWZframe_WLZL;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZWZframe_WLZH;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZWZframe_WHZL;
    Histo1DPtr h_Cut3_DeltaPhiLepWLepZWZframe_WHZH;
    Histo1DPtr h_Cut3_all_Lep_pT;
    Histo1DPtr h_Cut3_all_Lep_Eta;
    Histo1DPtr h_Cut3_all_Lep_Phi;

    //@}
    
    const vector<double> pT_Z_CUTS = {0, 30, 60, 90, 120, 150, 220, 2000};

    double MZ_PDG = 91.1876;
    double MW_PDG = 83.385;
    double GammaZ_PDG = 2.4952;
    double GammaW_PDG = 2.085;

  private:
    enum PhaseSpaceSelections {
      ps_PreFilter,
      ps_All,
      ps_Lep,
      ps_ptZ,
      ps_R21,
      ps_NCuts
    };

    enum channelList {
      cl_eee,
      cl_mee,
      cl_emm,
      cl_mmm,
      cl_NChannels
    };

    map<PhaseSpaceSelections, int> _processedEvents;
  
    int _eventsPassCuts[ps_NCuts];

    std::ofstream m_strOutput;

  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_WZ_00enhanced_emu);
}
